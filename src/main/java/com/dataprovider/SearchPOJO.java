package com.dataprovider;

public class SearchPOJO {
	
	private String searchFor;
	

	public SearchPOJO() {
		super();
	}

	public SearchPOJO(String searchFor) {
		super();
		this.searchFor = searchFor;
	}

	public String getSearchFor() {
		return searchFor;
	}

	public void setSearchFor(String searchFor) {
		this.searchFor = searchFor;
	}

	@Override
	public String toString() {
		return searchFor;
	}
	
	

}
